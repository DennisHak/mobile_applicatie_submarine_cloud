import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/route_generator.dart';
import 'package:asd/themes/theme.dart';
import 'package:flutter/material.dart';
import 'package:injector/injector.dart';

import 'modules/connection/database_connection.dart';

const String host = 'asd-p1-server1.asd.icaprojecten.nl';
const int port = 25010;

void main() {
  injectorBootstrap();
  runApp(Main());

  final Injector injector = Injector.appInstance;
  final DatabaseConnection _databaseConnection =
      injector.getDependency<DatabaseConnection>();

  _databaseConnection.initDatabase().then((_) async {
    final ControllerModule controllerModule =
        injector.getDependency<ControllerModule>();
    controllerModule.getDependencies();
    await controllerModule.startServer();
  });
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Confidential Messenger',
      theme: mainTheme,
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
