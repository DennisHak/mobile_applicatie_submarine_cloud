import 'package:asd/bloc/message/bloc.dart';
import 'package:asd/components/chat/message_form.dart';
import 'package:asd/components/chat/message_overview.dart';
import 'package:asd/constants.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persist_contacts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';
import 'package:pointycastle/export.dart';

class ChatConversation extends StatefulWidget {
  const ChatConversation({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ChatConversationPageState createState() => _ChatConversationPageState();
}

class _ChatConversationPageState extends State<ChatConversation> {
  final ControllerModule controllerModule =
      Injector.appInstance.getDependency<ControllerModule>();
  final PersistContacts _persistContacts =
      Injector.appInstance.getDependency<PersistContacts>();
  final ScrollController _scrollController = ScrollController();

  String userAlias;
  MessageBloc _messageBloc;
  Contact receiverInfo;

  @override
  void initState() {
    super.initState();
    _initMessageBloc();
    _initMockReceiver();
    _persistContacts
        .getUserData()
        .then((Contact user) => setState(() => userAlias = user.alias));
  }

  void _initMockReceiver() {
    final AsymmetricEncryption encryptor =
        Injector.appInstance.getDependency<AsymmetricEncryption>();
    final AsymmetricKeyPair<PublicKey, PrivateKey> keyPair =
        encryptor.generateRandomKeyPair();
    receiverInfo = Contact('receiver', keyPair.publicKey);
  }

  void _initMessageBloc() {
    _messageBloc = BlocProvider.of<MessageBloc>(context);
    _messageBloc.add(FetchMessages());
    Injector.appInstance.removeByKey<MessageBloc>();
    Injector.appInstance.registerSingleton<MessageBloc>((_) => _messageBloc);
  }

  Future<void> _sendMessageToFirstNode(String message) async {
    if (message.isNotEmpty && userAlias.isNotEmpty) {
      final TextMessage textMessage = TextMessage(
        userAlias,
        DateTime.now(),
        'conversationId',
        message,
      );

      controllerModule.sendMessage(textMessage, receiverInfo);
      _messageBloc.add(IncomingMessage(message: textMessage));
      if (_scrollController.hasClients) {
        _scrollController.animateTo(
          0.0,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 300),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGainsboro,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Stack(
          fit: StackFit.loose,
          children: <Widget>[
            MessageOverview(
              controller: _scrollController,
              userAlias: userAlias,
            ),
            MessageForm(messageHandler: _sendMessageToFirstNode)
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
