import 'package:asd/bloc/message/bloc.dart';
import 'package:asd/modules/chucking/chunking_module_impl.dart';
import 'package:asd/modules/connection/connection_module_impl.dart';
import 'package:asd/modules/connection/database_connection.dart';
import 'package:asd/modules/connection/send_message_impl.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/modules/encryption/asymmetric_encryption_impl.dart';
import 'package:asd/modules/encryption/encryption_module_impl.dart';
import 'package:asd/modules/encryption/symmetric_encryption_impl.dart';
import 'package:asd/modules/persistence/persist_contacts_impl.dart';
import 'package:asd/modules/persistence/persist_conversations_impl.dart';
import 'package:asd/modules/persistence/persist_messages_impl.dart';
import 'package:asd/modules/persistence/persistence_module_impl.dart';
import 'package:asd/modules/routing/node_information_impl.dart';
import 'package:asd/modules/routing/route_module_impl.dart';
import 'package:asd/modules/shared/chunking/chunking_module.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/connection/send_message.dart';
import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:asd/modules/shared/encryption/encryption_module.dart';
import 'package:asd/modules/shared/encryption/symmetric_encryption.dart';
import 'package:asd/modules/shared/persistence/persist_contacts.dart';
import 'package:asd/modules/shared/persistence/persist_conversations.dart';
import 'package:asd/modules/shared/persistence/persist_messages.dart';
import 'package:asd/modules/shared/persistence/persistence_module.dart';
import 'package:asd/modules/shared/routing/node_information.dart';
import 'package:injector/injector.dart';

import 'modules/shared/routing/route_module.dart';

void injectorBootstrap() {
  final Injector injector = Injector.appInstance;

  // Bloc
  injector.registerSingleton<MessageBloc>((_) => null);

  // Connection
  injector.registerSingleton<ConnectionModule>((_) => ConnectionModuleImpl());
  injector.registerSingleton<SendMessage>((_) => SendMessageImpl());
  injector.registerSingleton<NodeInformation>((_) => NodeInformationImpl());
  injector.registerSingleton<DatabaseConnection>((_) => DatabaseConnection());

  // Encryption
  injector.registerDependency<EncryptionModule>((_) => EncryptionModuleImpl());
  injector.registerDependency<AsymmetricEncryption>(
      (_) => AsymmetricEncryptionImpl());
  injector.registerDependency<SymmetricEncryption>(
      (_) => SymmetricEncryptionImpl());

  // FileSplitter
  injector.registerDependency<ChunkingModule>((_) => ChunkingModuleImpl());

  // MessageHandler
  injector.registerSingleton<ControllerModule>((_) => ControllerModule());

  // Persistence
  injector.registerSingleton<PersistenceModule>((_) => PersistenceModuleImpl());
  injector.registerSingleton<PersistContacts>((_) => PersistContactsImpl());
  injector.registerSingleton<PersistConversations>(
      (_) => PersistConversartionImpl());
  injector.registerSingleton<PersistMessages>((_) => PersistMessagesImpl());

  //Routing
  injector.registerSingleton<RouteModule>((_) => RouteModuleImpl());
}
