import 'package:asd/bloc/message/bloc.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'chat_bubble.dart';

class MessageOverview extends StatelessWidget {
  const MessageOverview({
    Key key,
    @required this.controller,
    @required this.userAlias,
  }) : super(key: key);

  final ScrollController controller;
  final String userAlias;

  Widget loadingScreen() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SpinKitFadingCircle(
          color: Colors.black,
          size: 50.0,
        ),
        const SizedBox(
          height: 20.0,
        ),
        Text(
          'Loading messages...',
          style: TextStyle(
            fontFamily: 'Nunito',
            fontSize: 16.0,
            color: Colors.black,
          ),
        )
      ],
    );
  }

  Widget messageOverview({MessageLoaded state}) {
    final List<Message> messages = state.messages;
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        final Message message = messages[index];
        return ChatBubble(
          message: message,
          isSender: message.sender == userAlias,
        );
      },
      itemCount: messages.length,
      padding: const EdgeInsets.only(
        top: 10.0,
        left: 8.0,
        right: 8.0,
        bottom: 60.0,
      ),
      scrollDirection: Axis.vertical,
      reverse: true,
      controller: controller,
    );
  }

  Widget errorDisplay() {
    return const Center(
      child: Text('failed to load state'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MessageBloc, MessageState>(
        builder: (BuildContext context, MessageState state) {
      if (state is MessageUninitialized || userAlias == null) {
        return loadingScreen();
      } else if (state is MessageLoaded) {
        return messageOverview(state: state);
      } else {
        return errorDisplay();
      }
    });
  }
}
