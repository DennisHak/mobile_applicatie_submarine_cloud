import 'package:asd/constants.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const double kBubbleMarginTop = 8.0;
const double kBubbleMarginSide = 50.0;
const double kBubblePaddingVertical = 10.0;
const double kBubblePaddingHorizontal = 20.0;
const double kBubbleInfoTextPaddingSide = 8.0;
const double kBubbleInfoTextPaddingBottom = 5.0;

class ChatBubble extends StatelessWidget {
  const ChatBubble({Key key, @required this.message, @required this.isSender})
      : super(key: key);

  final bool isSender;
  final Message message;

  BubbleStyle _getBubbleStyle(double pixelRatio) {
    final double px = 1 / pixelRatio;
    return BubbleStyle(
      nip: isSender ? BubbleNip.rightBottom : BubbleNip.leftBottom,
      color: isSender ? kSilverSand : kMunsellBlue,
      elevation: 1 * px,
      alignment: isSender ? Alignment.topRight : Alignment.topLeft,
      padding: const BubbleEdges.symmetric(
        vertical: kBubblePaddingVertical,
        horizontal: kBubblePaddingHorizontal,
      ),
    );
  }

  EdgeInsets getMargin() {
    if (isSender) {
      return const EdgeInsets.only(
        top: kBubbleMarginTop,
        left: kBubbleMarginSide,
      );
    }
    return const EdgeInsets.only(
      top: kBubbleMarginTop,
      right: kBubbleMarginSide,
    );
  }

  String getDateString() {
    final DateTime now = DateTime.now();
    final DateTime today = DateTime(now.year, now.month, now.day);
    final DateTime yesterday = DateTime(now.year, now.month, now.day - 1);

    final DateTime messageDate = DateTime(
      message.timestamp.year,
      message.timestamp.month,
      message.timestamp.day,
    );

    String day;
    if (messageDate == today) {
      day = 'today';
    } else if (messageDate == yesterday) {
      day = 'yesterday';
    } else {
      day = DateFormat('dd-MM-yyyy').format(message.timestamp);
    }
    return '$day at ${DateFormat('Hm').format(message.timestamp)}';
  }

  Widget infoText() {
    EdgeInsets padding;

    if (isSender) {
      padding = const EdgeInsets.only(
        bottom: kBubbleInfoTextPaddingBottom,
        right: kBubbleInfoTextPaddingSide,
      );
    } else {
      padding = const EdgeInsets.only(
        bottom: kBubbleInfoTextPaddingBottom,
        left: kBubbleInfoTextPaddingSide,
      );
    }

    return Padding(
      padding: padding,
      child: Text(
        '${isSender ? '' : message.sender} (${getDateString()})',
        style: TextStyle(
          fontFamily: 'Nunito',
          color: Colors.black,
        ),
      ),
    );
  }

  Bubble chatBubble({double pixelRatio}) {
    return Bubble(
      style: _getBubbleStyle(pixelRatio),
      child: Text(
        (message as TextMessage).message,
        style: TextStyle(
            fontFamily: 'Nunito',
            fontSize: 16.0,
            color: isSender ? Colors.black : Colors.white),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double pixelRatio = MediaQuery.of(context).devicePixelRatio;
    return Container(
      margin: getMargin(),
      child: Column(
        crossAxisAlignment:
            isSender ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          infoText(),
          chatBubble(pixelRatio: pixelRatio),
        ],
      ),
    );
  }
}
