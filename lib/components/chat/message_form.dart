import 'package:asd/constants.dart';
import 'package:flutter/material.dart';

class MessageForm extends StatelessWidget {
  MessageForm({Key key, this.messageHandler}) : super(key: key);

  final Future<void> Function(String) messageHandler;
  final TextEditingController _messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      right: 0,
      left: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(height: 0, color: Colors.black26),
          Container(
            color: kDavyGray,
            height: 50,
            child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: textFieldWidget()),
          ),
        ],
      ),
    );
  }

  TextField textFieldWidget() {
    return TextField(
      controller: _messageController,
      maxLines: 20,
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
        prefixIcon: fileButtonWidget(),
        suffixIcon: sendMessageButtonWidget(),
        border: InputBorder.none,
        hintText: 'Bericht...',
        hintStyle: TextStyle(color: Colors.grey),
      ),
    );
  }

  IconButton fileButtonWidget() {
    return IconButton(
      icon: Icon(Icons.insert_drive_file, color: kMunsellBlue),
      onPressed: () {},
    );
  }

  IconButton sendMessageButtonWidget() {
    return IconButton(
        key: const Key('SendMessageButton'),
        icon: Icon(Icons.send, color: kMunsellBlue),
        onPressed: () async {
          await messageHandler(_messageController.text);
          _messageController.clear();
        });
  }
}
