import 'package:asd/modules/shared/models/message.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class MessageEvent extends Equatable {
  @override
  List<Object> get props => <Object>[];
}

class FetchMessages extends MessageEvent {}

class IncomingMessage extends MessageEvent {
  IncomingMessage({@required this.message}) : assert(message != null);

  final Message message;
}
