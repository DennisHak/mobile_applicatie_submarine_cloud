import 'package:asd/bloc/message/bloc.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/persistence/persist_messages.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';

class MessageBloc extends Bloc<MessageEvent, MessageState> {
  @override
  MessageState get initialState => MessageUninitialized();

  @override
  Stream<MessageState> mapEventToState(MessageEvent event) async* {
    if (event is FetchMessages) {
      yield* _mapFetchMessagesToState(event);
    } else if (event is IncomingMessage) {
      yield* _mapIncomingMessageToState(event);
    }
  }

  Stream<MessageState> _mapFetchMessagesToState(FetchMessages event) async* {
    try {
      final List<Message> messages = await _fetchMessages();
      //final List<Message> messages = _fetchMessages();
      yield MessageLoaded(messages: messages);
    } catch (_) {
      yield MessageError();
    }
  }

  Stream<MessageState> _mapIncomingMessageToState(
      IncomingMessage event) async* {
    if (state is MessageLoaded) {
      final MessageLoaded currentState = state as MessageLoaded;
      try {
        if (event is IncomingMessage) {
          final List<Message> newMessages =
              List<Message>.from(currentState.messages)
                ..insert(0, event.message);
          yield MessageLoaded(messages: newMessages);
          return;
        }
      } catch (_) {
        yield MessageError();
      }
    }
  }

  Future<List<Message>> _fetchMessages() async {
    final PersistMessages persistMessages =
        Injector.appInstance.getDependency<PersistMessages>();
    return persistMessages.getMessageList('conversationId');
  }
}
