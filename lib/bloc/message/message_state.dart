import 'package:asd/modules/shared/models/message.dart';
import 'package:equatable/equatable.dart';

abstract class MessageState extends Equatable {
  const MessageState();

  @override
  List<Object> get props => <Object>[];
}

class MessageUninitialized extends MessageState {}

class MessageError extends MessageState {}

class MessageLoaded extends MessageState {
  const MessageLoaded({this.messages});

  final List<Message> messages;

  MessageLoaded copyWith({List<Message> messages}) {
    return MessageLoaded(
      messages: messages ?? this.messages,
    );
  }

  @override
  List<Object> get props => <Object>[messages];

  @override
  String toString() => 'MessageLoaded { messages: ${messages.length} }';
}
