import 'dart:ui';

const Color kMunsellBlue = Color(0xFF1985A1);
const Color kBlackCoral = Color(0xFF4C5C68);
const Color kDavyGray = Color(0xFF46494C);
const Color kSilverSand = Color(0xFFC5C3C6);
const Color kGainsboro = Color(0xFFDCDCDD);
