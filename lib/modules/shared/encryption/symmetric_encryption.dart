import 'dart:typed_data';

import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/path.dart';
import 'package:encrypt/encrypt.dart';

abstract class SymmetricEncryption {
  Uint8List decryptSymmetric(String contentToDecrypt, Key symmetricKey);
  String encryptSymmetric(dynamic contentToEncrypt, Key symmetricKey);
  Key generateRandomSymmetricKey();
  Onion encryptOnion(Path path, Message message, Contact receiver);
}
