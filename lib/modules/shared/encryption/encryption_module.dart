import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:asd/modules/shared/encryption/symmetric_encryption.dart';

abstract class EncryptionModule
    implements AsymmetricEncryption, SymmetricEncryption {}
