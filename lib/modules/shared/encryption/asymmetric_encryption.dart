import 'dart:typed_data';

import 'package:pointycastle/api.dart';

abstract class AsymmetricEncryption {
  Uint8List decryptAsymmetric(
      Uint8List contentToDecrypt, PrivateKey privateKey);
  Uint8List encryptAsymmetric(Uint8List contentToEncrypt, PublicKey publicKey);
  AsymmetricKeyPair<PublicKey, PrivateKey> generateRandomKeyPair();
}
