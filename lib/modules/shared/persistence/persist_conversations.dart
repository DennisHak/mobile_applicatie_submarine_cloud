import 'package:asd/modules/shared/models/conversation.dart';

abstract class PersistConversations {
  Future<void> insertConversation(Conversation conversation);
  Future<void> updateConversation(Conversation conversation);
  Future<void> deleteConversation(Conversation conversation);
  Future<Conversation> getConversation(int conversationId);
  Future<List<Conversation>> getConversationList();
}
