import 'package:asd/modules/shared/models/contact.dart';

abstract class PersistContacts {
  Future<void> insertContact(Contact contact);
  Future<void> updateContact(Contact contact);
  Future<void> deleteContact(Contact contact);
  Future<Contact> getContact(int contactId);
  Future<List<Contact>> getContactList();
  Future<Contact> getUserData();
}
