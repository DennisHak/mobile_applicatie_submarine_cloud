import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/text_message.dart';

abstract class PersistMessages {
  Future<void> insertTextMessage(TextMessage message);
  Future<List<Message>> getMessageList(String conversationId);
}
