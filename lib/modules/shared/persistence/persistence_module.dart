import 'package:asd/modules/shared/models/message.dart';

abstract class PersistenceModule {
  Future<void> insertMessage(Message message);
}
