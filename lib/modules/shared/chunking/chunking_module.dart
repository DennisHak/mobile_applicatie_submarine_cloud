import 'dart:io';

abstract class ChunkingModule {
  Future<List<File>> chunkFile(File file);
  File mergeFile(List<File> files, File destination);
}
