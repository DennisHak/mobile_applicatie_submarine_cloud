abstract class SocketServer {
  Future<void> startServer();

  Future<void> stopServer();
}
