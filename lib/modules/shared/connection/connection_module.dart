import 'package:asd/modules/shared/models/onion.dart';

abstract class ConnectionModule {
  Future<void> connect(String host, int port);
  Future<void> sendMessage(Onion onion);
}
