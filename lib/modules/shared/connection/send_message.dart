import 'dart:io';

import 'package:asd/modules/shared/models/onion.dart';

abstract class SendMessage {
  Future<void> sendMessage(Socket socket, Onion onion);
}
