import 'package:asd/modules/shared/models/node.dart';

abstract class NodeInformation {
  Future<List<Node>> getNodeInfo();
  Node create(String hostname, String key);
}
