import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/path.dart';

abstract class RouteModule {
  Future<Path> calculateRoute(int lengthOfPath);
  Future<List<Path>> calculateRoutes(int numberOfRoutes, int lengthOfPath);
  Future<Onion> makeOnion(Message message, Contact receiver);
}
