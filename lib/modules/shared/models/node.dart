import 'package:asd/modules/shared/models/destination_relay.dart';

class Node {
  //Test implementation of the class Node
  Node(this._destination, this._key);

  final DestinationRelay _destination;
  final String _key;

  DestinationRelay get destination => _destination;
  String get key => _key;
}
