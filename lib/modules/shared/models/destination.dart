import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';

class Destination {
  Destination fromJson(Map<String, dynamic> json) {
    if (json['alias'] != null) {
      return DestinationClient.fromJson(json);
    } else {
      return DestinationRelay.fromJson(json);
    }
  }
}
