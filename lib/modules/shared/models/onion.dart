import 'package:asd/modules/shared/models/destination.dart';

class Onion {
  Onion(this._destination, this._command, this._data);

  Onion.fromJson(Map<String, dynamic> json)
      : _destination =
            Destination().fromJson(json['destination'] as Map<String, dynamic>),
        _command = json['command'].toString(),
        _data = json['data'].toString();

  final Destination _destination;
  final String _command;
  final String _data;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'destination': _destination,
      'command': _command,
      'data': _data,
    };
  }

  Destination get destination => _destination;
  String get data => _data;
  String get command => _command;
}
