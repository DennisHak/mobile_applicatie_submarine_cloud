import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/message.dart';

class Conversation {
  Conversation(this._id, this._participants, this._messages);

  final String _id;
  final List<Contact> _participants;
  final List<Message> _messages;

  String get id => _id;
  List<Contact> get participants => _participants;
  List<Message> get messages => _messages;
}
