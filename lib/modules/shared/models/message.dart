import 'package:equatable/equatable.dart';

abstract class Message extends Equatable {
  const Message(
    this._sender,
    this._timestamp,
    this._conversationId,
  );

  final String _sender;
  final DateTime _timestamp;
  final String _conversationId;

  String get sender => _sender;
  DateTime get timestamp => _timestamp;
  String get conversationId => _conversationId;

  Map<String, dynamic> toJson();

  @override
  List<Object> get props => <Object>[_sender, _timestamp, _conversationId];

  @override
  String toString() => 'message { sender: $_sender, '
      'timestamp: $_timestamp, '
      'conversationId: $_conversationId }';
}
