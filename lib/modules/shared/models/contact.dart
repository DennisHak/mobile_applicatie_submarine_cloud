import 'package:pointycastle/export.dart';

class Contact {
  const Contact(this._alias, this._publicKey);

  final String _alias;
  final PublicKey _publicKey;

  String get alias => _alias;
  PublicKey get publicKey => _publicKey;
}
