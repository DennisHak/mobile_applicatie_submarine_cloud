import 'package:asd/modules/shared/models/message.dart';

class TextMessage extends Message {
  const TextMessage(
    String sender,
    DateTime timestamp,
    String conversationId,
    this._message,
  ) : super(sender, timestamp, conversationId);

  final String _message;
  String get message => _message;

  @override
  List<Object> get props => List<Object>.from(super.props)..add(message);

  @override
  String toString() => 'message { sender: ${super.sender}, '
      'timestamp: ${super.timestamp}, '
      'conversationId: ${super.conversationId}, '
      'message: $_message }';

  @override
  Map<String, dynamic> toJson() => <String, dynamic>{
        'sender': sender,
        'timestamp': timestamp.toString(),
        'conversationId': conversationId,
        'message': message
      };
}
