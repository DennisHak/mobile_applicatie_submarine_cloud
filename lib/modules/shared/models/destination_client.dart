import 'package:asd/modules/shared/models/destination.dart';

class DestinationClient extends Destination {
  DestinationClient(this._alias, this._encryptedSymmetricKey);

  DestinationClient.fromJson(Map<String, dynamic> json)
      : _alias = json['alias'].toString(),
        _encryptedSymmetricKey = json['encryptedSymmetricKey'].toString();

  final String _alias;
  final String _encryptedSymmetricKey;

  Map<String, dynamic> toJson() => <String, dynamic>{
        'alias': _alias,
        'encryptedSymmetricKey': _encryptedSymmetricKey
      };

  String get alias => _alias;
  String get encryptedSymmetricKey => _encryptedSymmetricKey;

  @override
  String toString() {
    return toJson().toString();
  }
}
