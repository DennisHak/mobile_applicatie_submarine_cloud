import 'package:asd/modules/shared/models/node.dart';

class Path {
  final List<Node> _nodes = <Node>[];

  List<Node> getNodes() {
    return _nodes;
  }

  void addNodeToPath(Node node) {
    _nodes.add(node);
  }
}
