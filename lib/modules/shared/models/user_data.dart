class UserData {
  UserData(this._alias, this._publicKey, this._privateKey);

  final String _alias;
  final String _publicKey;
  final String _privateKey;

  String get alias => _alias;
  String get publicKey => _publicKey;
  String get privateKey => _privateKey;
}
