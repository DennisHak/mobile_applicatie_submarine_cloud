import 'package:asd/modules/shared/models/destination.dart';

class DestinationRelay extends Destination {
  DestinationRelay(this._host, this._port);

  DestinationRelay.fromJson(Map<String, dynamic> json)
      : _host = json['hostname'].toString(),
        _port = int.parse(json['port'].toString());

  final String _host;
  final int _port;

  Map<String, dynamic> toJson() =>
      <String, dynamic>{'hostname': _host, 'port': _port};

  int get port => _port;
  String get hostname => _host;

  @override
  String toString() {
    return toJson().toString();
  }
}
