import 'package:asd/modules/shared/models/onion.dart';

abstract class MessageHandler {
  void incomingMessage(Onion onion);
}
