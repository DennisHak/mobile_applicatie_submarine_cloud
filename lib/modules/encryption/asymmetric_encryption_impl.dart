import 'dart:math';
import 'dart:typed_data';

import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:pointycastle/export.dart';

class AsymmetricEncryptionImpl implements AsymmetricEncryption {
  //Methods are derived from: https://github.com/PointyCastle/pointycastle/blob/master/tutorials/rsa.md

  @override
  Uint8List decryptAsymmetric(
      Uint8List contentToDecrypt, PrivateKey privateKey) {
    try {
      final OAEPEncoding decrypt = OAEPEncoding(RSAEngine())
        ..init(false,
            PrivateKeyParameter<RSAPrivateKey>(privateKey)); // false=decrypt
      return _processInBlocks(decrypt, contentToDecrypt);
    } catch (error) {
      throw StateError('decryption failed');
    }
  }

  @override
  Uint8List encryptAsymmetric(Uint8List contentToEncrypt, PublicKey publicKey) {
    try {
      final OAEPEncoding encrypt = OAEPEncoding(RSAEngine())
        ..init(
            true, PublicKeyParameter<RSAPublicKey>(publicKey)); // true=encrypt
      return _processInBlocks(encrypt, contentToEncrypt);
    } catch (error) {
      throw StateError('encryption failed');
    }
  }

  Uint8List _processInBlocks(AsymmetricBlockCipher engine, Uint8List input) {
    final int numBlocks = input.length ~/ engine.inputBlockSize +
        ((input.length % engine.inputBlockSize != 0) ? 1 : 0);

    final Uint8List output = Uint8List(numBlocks * engine.outputBlockSize);

    int inputOffset = 0;
    int outputOffset = 0;
    while (inputOffset < input.length) {
      final int chunkSize =
          (inputOffset + engine.inputBlockSize <= input.length)
              ? engine.inputBlockSize
              : input.length - inputOffset;

      outputOffset += engine.processBlock(
          input, inputOffset, chunkSize, output, outputOffset);

      inputOffset += chunkSize;
    }

    return (output.length == outputOffset)
        ? output
        : output.sublist(0, outputOffset);
  }

  @override
  AsymmetricKeyPair<PublicKey, PrivateKey> generateRandomKeyPair() {
    final KeyGenerator keyGen = KeyGenerator('RSA');
    final RSAKeyGeneratorParameters rsaParams =
        RSAKeyGeneratorParameters(BigInt.parse('65537'), 2048, 64);
    final ParametersWithRandom<RSAKeyGeneratorParameters> paramsWithRnd =
        ParametersWithRandom<RSAKeyGeneratorParameters>(
            rsaParams, _RSASecureRandom());
    keyGen.init(paramsWithRnd);
    return keyGen.generateKeyPair();
  }

  SecureRandom _RSASecureRandom() {
    final FortunaRandom secureRandom = FortunaRandom();
    final Random seedSource = Random.secure();
    final List<int> seeds = <int>[];
    for (int i = 0; i < 32; i++) {
      seeds.add(seedSource.nextInt(255));
    }
    secureRandom.seed(KeyParameter(Uint8List.fromList(seeds)));
    return secureRandom;
  }
}
