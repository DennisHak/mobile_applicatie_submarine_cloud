import 'dart:convert';
import 'dart:typed_data';

import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:asd/modules/shared/encryption/symmetric_encryption.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/destination.dart';
import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/node.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/path.dart';
import 'package:encrypt/encrypt.dart';
import 'package:injector/injector.dart';

class SymmetricEncryptionImpl implements SymmetricEncryption {
  final IV kIV = IV.fromUtf8('0705090625458532');
  List<Key> listOfEncryptionKeys = <Key>[];

  @override
  Uint8List decryptSymmetric(String contentToDecrypt, Key symmetricKey) {
    if (symmetricKey == null) {
      throw ArgumentError('Symmetric key is null');
    }
    if (contentToDecrypt == null) {
      throw ArgumentError('Content to decrypten is null');
    }
    final Encrypter AESEncrypter =
        Encrypter(AES(symmetricKey, mode: AESMode.cbc));
    final Encrypted encrypted = Encrypted.fromBase64(contentToDecrypt);
    return Uint8List.fromList(AESEncrypter.decryptBytes(encrypted, iv: kIV));
  }

  @override
  String encryptSymmetric(dynamic contentToEncrypt, Key symmetricKey) {
    if (symmetricKey == null) {
      throw ArgumentError('Symmetric key is null');
    }
    if (contentToEncrypt == null) {
      throw ArgumentError('Content to encrypt is null');
    }
    final Encrypter AESEncrypter =
        Encrypter(AES(symmetricKey, mode: AESMode.cbc));

    Uint8List bytes;
    if (contentToEncrypt is Onion) {
      final String content = jsonEncode(contentToEncrypt);
      bytes = const Utf8Encoder().convert(content);
    } else if (contentToEncrypt is String) {
      bytes = const Utf8Encoder().convert(contentToEncrypt);
    } else {
      throw ArgumentError('Content to encrypt is not an Onion');
    }

    final Encrypted encrypted = AESEncrypter.encryptBytes(bytes, iv: kIV);
    return encrypted.base64;
  }

  @override
  Key generateRandomSymmetricKey() {
    return Key.fromLength(32);
  }

  @override
  Onion encryptOnion(Path path, Message message, Contact receiver) {
    if (path.getNodes().length <= 1) {
      throw ArgumentError('There are no nodes to encrypt onion');
    } else {
      final AsymmetricEncryption encryptor =
          Injector.appInstance.getDependency<AsymmetricEncryption>();

      final Key symmetricKey = generateRandomSymmetricKey();
      final String base64EncodedKey = const Base64Encoder().convert(
        encryptor.encryptAsymmetric(
          symmetricKey.bytes,
          receiver.publicKey,
        ),
      );

      final DestinationClient destination = DestinationClient(
        receiver.alias,
        base64EncodedKey,
      );

      final String json = jsonEncode(message);
      final String encryptedJson = encryptSymmetric(json, symmetricKey);

      return constructOnionRecursively(
          destination, <Node>[...path.getNodes()], encryptedJson, null);
    }
  }

  Onion constructOnionRecursively(Destination destination, List<Node> nodes,
      String data, Node previousNode) {
    final Node currentNode = nodes.removeAt(nodes.length - 1);

    final Onion onion = Onion(
      previousNode == null ? destination : createDestinationRelay(previousNode),
      'RELAY',
      data,
    );
    final Key secretKey = convertBase64StringToKey(currentNode.key);
    final String encryptedData = encryptSymmetric(onion, secretKey);

    if (nodes.isEmpty) {
      return Onion(
        createDestinationRelay(currentNode),
        'RELAY',
        encryptedData,
      );
    } else {
      return constructOnionRecursively(null, nodes, encryptedData, currentNode);
    }
  }

  DestinationRelay createDestinationRelay(Node currentNode) {
    return DestinationRelay(
        currentNode.destination.hostname, currentNode.destination.port);
  }

  Key convertBase64StringToKey(String key) {
    return Key.fromBase64(key);
  }
}
