import 'dart:typed_data';

import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:asd/modules/shared/encryption/encryption_module.dart';
import 'package:asd/modules/shared/encryption/symmetric_encryption.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/path.dart';
import 'package:encrypt/encrypt.dart';
import 'package:injector/injector.dart';
import 'package:pointycastle/api.dart';

class EncryptionModuleImpl implements EncryptionModule {
  EncryptionModuleImpl() {
    final Injector injector = Injector.appInstance;
    _asymmetricEncryption = injector.getDependency<AsymmetricEncryption>();
    _symmetricEncryption = injector.getDependency<SymmetricEncryption>();
  }

  AsymmetricEncryption _asymmetricEncryption;
  SymmetricEncryption _symmetricEncryption;

  @override
  Uint8List decryptAsymmetric(
      Uint8List contentToDecrypt, PrivateKey privateKey) {
    return _asymmetricEncryption.decryptAsymmetric(
        contentToDecrypt, privateKey);
  }

  @override
  Uint8List encryptAsymmetric(Uint8List contentToEncrypt, PublicKey publicKey) {
    return _asymmetricEncryption.encryptAsymmetric(contentToEncrypt, publicKey);
  }

  @override
  Uint8List decryptSymmetric(String contentToDecrypt, Key symmetricKey) {
    return _symmetricEncryption.decryptSymmetric(
        contentToDecrypt, symmetricKey);
  }

  @override
  String encryptSymmetric(dynamic contentToEncrypt, Key symmetricKey) {
    return _symmetricEncryption.encryptSymmetric(
        contentToEncrypt, symmetricKey);
  }

  @override
  AsymmetricKeyPair<PublicKey, PrivateKey> generateRandomKeyPair() {
    return _asymmetricEncryption.generateRandomKeyPair();
  }

  @override
  Key generateRandomSymmetricKey() {
    return _symmetricEncryption.generateRandomSymmetricKey();
  }

  @override
  Onion encryptOnion(Path path, Message message, Contact receiver) {
    return _symmetricEncryption.encryptOnion(path, message, receiver);
  }
}
