import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/connection/socket_server.dart';
import 'package:asd/modules/shared/message/message_handler.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:injector/injector.dart';
import 'package:logger/logger.dart';

const int kSocketServerPort = 25010;

class SocketServerImpl extends SocketServer {
  SocketServerHandler socketServerHandler;

  @override
  Future<void> startServer() async {
    final ServerSocket socketServer = await ServerSocket.bind(
      InternetAddress.anyIPv4,
      kSocketServerPort,
      shared: true,
    );
    socketServerHandler = SocketServerHandler(socketServer);
  }

  @override
  Future<void> stopServer() async {
    await socketServerHandler.stopServer();
  }
}

class SocketServerHandler {
  SocketServerHandler(this._socketServer) {
    _socketServer.listen(_handleClient);
  }

  final Logger _logger = Logger();
  final ServerSocket _socketServer;
  final List<Socket> _socketClients = <Socket>[];
  final MessageHandler _messageHandler =
      Injector.appInstance.getDependency<ConnectionModule>() as MessageHandler;

  Future<void> stopServer() async {
    _disconnectClients();
    await _socketServer.close();
  }

  void _handleClient(Socket clientConnection) {
    _socketClients.add(clientConnection);

    clientConnection.listen(
      _handleMessage,
      onError: (dynamic error) => _handleError(clientConnection, error),
      onDone: () => _handleDone(clientConnection),
    );
  }

  void _handleMessage(Uint8List onData) {
    final Onion onion = _convertToOnion(onData);
    if (onion != null) {
      _messageHandler.incomingMessage(onion);
    }
  }

  void _handleError(Socket clientConnection, dynamic error) {
    _logger.i('Error with incoming socket connection; $error');
    _handleDone(clientConnection);
  }

  void _handleDone(Socket clientConnection) {
    // TODO: implement handle connection close
    _disconnectClient(clientConnection);
  }

  Onion _convertToOnion(Uint8List onData) {
    try {
      final String rawMessage = String.fromCharCodes(onData);
      final Map<String, dynamic> json =
          jsonDecode(rawMessage) as Map<String, dynamic>;
      return Onion.fromJson(json);
    } catch (e) {
      _logger.d('Error converting message to onion; $e');
    }
    return null;
  }

  void _disconnectClients() {
    for (final Socket clientConnection in _socketClients) {
      _disconnectClient(clientConnection);
    }
  }

  void _disconnectClient(Socket clientConnection) {
    if (clientConnection != null) {
      clientConnection.close();
      clientConnection.destroy();
    }
    _socketClients.remove(clientConnection);
  }
}
