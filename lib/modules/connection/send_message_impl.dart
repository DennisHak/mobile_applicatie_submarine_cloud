import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:asd/modules/shared/connection/send_message.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:logger/logger.dart';

class SendMessageImpl implements SendMessage {
  final Logger _logger = Logger();

  @override
  Future<void> sendMessage(Socket socket, Onion onion) async {
    try {
      String message = jsonEncode(onion);
      message = '${message.length}$message';
      socket.write(message);
      await socket.flush();
    } on IOException catch (exception) {
      _logger.d('$exception');

      try {
        socket.close();
        // ignore: empty_catches
      } catch (IOException) {
        _logger.d('IOException occured; $IOException');
      }

      rethrow;
    }
  }
}
