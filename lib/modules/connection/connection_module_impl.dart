import 'dart:io';

import 'package:asd/modules/connection/socket_server_impl.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/connection/send_message.dart';
import 'package:asd/modules/shared/connection/socket_server.dart';
import 'package:asd/modules/shared/message/message_handler.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:injector/injector.dart';
import 'package:logger/logger.dart';

const int kSocketConnectionTimeout = 10;

class ConnectionModuleImpl
    implements ConnectionModule, MessageHandler, SocketServer {
  final ControllerModule _controllerModule =
      Injector.appInstance.getDependency<ControllerModule>();
  final SendMessage _sendMessage =
      Injector.appInstance.getDependency<SendMessage>();

  Socket _socket;
  SocketServer _socketServer;
  final Logger _logger = Logger();

  @override
  Future<void> connect(String host, int port) async {
    _logger.i('Initializing connection with the sockets');
    try {
      _socket = await Socket.connect(host, port,
          timeout: const Duration(seconds: kSocketConnectionTimeout));
      _logger.i('Connected to socket');
    } on SocketException catch (e) {
      _logger.i('Error creating socket connection; $e');
      rethrow;
    }
  }

  @override
  Future<void> startServer() async {
    _logger.i('Starting socket server');
    _socketServer = SocketServerImpl();
    await _socketServer.startServer();
    _logger.i('Started socket server');
  }

  @override
  Future<void> stopServer() async {
    await _socketServer.stopServer();
    _logger.i('Stopped socket server');
  }

  @override
  Future<void> sendMessage(Onion onion) async {
    try {
      if (onion == null) {
        throw const FormatException('Emtpy onion');
      }

      if (onion.destination is! DestinationRelay ||
          (onion.destination as DestinationRelay).hostname == null ||
          (onion.destination as DestinationRelay).port == null) {
        throw const FormatException('Invalid Destination');
      }

      final DestinationRelay destination =
          onion.destination as DestinationRelay;

      if (_socket == null ||
          (_socket.remoteAddress.host != destination.hostname ||
              _socket.remotePort != destination.port)) {
        if (_socket != null) {
          _socket.destroy();
        }
        await connect(destination.hostname, destination.port);
      }
      _sendMessage.sendMessage(_socket, onion);
    } catch (exception) {
      _logger.d('$exception');
      rethrow;
    }
  }

  @override
  void incomingMessage(Onion onion) {
    _controllerModule.incomingMessage(onion);
  }
}
