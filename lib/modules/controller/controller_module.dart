import 'dart:core';

import 'package:asd/bloc/message/message_bloc.dart';
import 'package:asd/bloc/message/message_event.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/connection/socket_server.dart';
import 'package:asd/modules/shared/message/message_handler.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persistence_module.dart';
import 'package:asd/modules/shared/routing/route_module.dart';
import 'package:injector/injector.dart';
import 'package:logger/logger.dart';

class ControllerModule implements MessageHandler {
  MessageBloc _messageBloc;
  ConnectionModule _connectionModule;
  PersistenceModule _persistenceModule;
  RouteModule _routeModule;
  final Logger _logger = Logger();

  void getDependencies() {
    final Injector injector = Injector.appInstance;
    _connectionModule = injector.getDependency<ConnectionModule>();
    _persistenceModule = injector.getDependency<PersistenceModule>();
    _routeModule = injector.getDependency<RouteModule>();
  }

  @override
  void incomingMessage(Onion onion) {
    final TextMessage textMessage = TextMessage(
        DestinationClient.fromJson(onion.toJson()).alias.toString(),
        DateTime.now(),
        'conversationId',
        onion.data);

    _persistenceModule.insertMessage(textMessage);
    if (_messageBloc != null) {
      _messageBloc.add(
        IncomingMessage(message: textMessage),
      );
    }
  }

  Future<void> sendMessage(Message message, Contact receiver) async {
    try {
      if (message is TextMessage) {
        await _persistenceModule.insertMessage(message);
        final Onion onion = await _routeModule.makeOnion(
          message,
          receiver,
        );
        await _connectionModule.sendMessage(onion);
      } else {
        throw UnimplementedError();
      }
    } catch (exception) {
      // TODO: implement deleting message

      _logger.i('Error sending message; $exception');
      throw Exception('Error sending message');
    }
  }

  Future<void> startServer() async {
    if (_connectionModule is SocketServer) {
      await (_connectionModule as SocketServer).startServer();
    }
  }
}
