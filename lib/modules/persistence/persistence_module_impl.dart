import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persist_messages.dart';
import 'package:asd/modules/shared/persistence/persistence_module.dart';
import 'package:couchbase_lite/couchbase_lite.dart';
import 'package:injector/injector.dart';

class PersistenceModuleImpl implements PersistenceModule {
  PersistenceModuleImpl() {
    final Injector injector = Injector.appInstance;
    _persistMessages = injector.getDependency<PersistMessages>();
  }

  PersistMessages _persistMessages;
  MutableDocument messages;
  MutableDocument conversations;
  MutableDocument contacts;

  @override
  Future<void> insertMessage(Message message) async {
    if (message is TextMessage) {
      await _persistMessages.insertTextMessage(message);
    }
  }
}
