import 'package:asd/modules/connection/database_connection.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persist_messages.dart';
import 'package:couchbase_lite/couchbase_lite.dart';
import 'package:flutter/services.dart';
import 'package:injector/injector.dart';
import 'package:logger/logger.dart';

class PersistMessagesImpl implements PersistMessages {
  PersistMessagesImpl() {
    final Injector injector = Injector.appInstance;
    _database = injector.getDependency<DatabaseConnection>().database;
  }

  final Logger _logger = Logger();
  Database _database;

  @override
  Future<List<TextMessage>> getMessageList(String conversationId) async {
    final List<TextMessage> returnValue = <TextMessage>[];

    final Where query = QueryBuilder.select(<SelectResultAs>[
      SelectResult.property('sender'),
      SelectResult.property('timestamp'),
      SelectResult.property('conversationId'),
      SelectResult.property('message'),
    ]).from('submarine').where(Expression.property('conversationId')
        .equalTo(Expression.string(conversationId)));

    try {
      final ResultSet results = await query.execute();
      _logger.i('Number of rows :: ${results.allResults().length}');

      for (final Result result in results) {
        final Map<String, dynamic> row = result.toMap();
        String sender;
        if (row['sender'] != null) {
          sender = row['sender'].toString();
        }
        returnValue.add(
          TextMessage(
            sender,
            DateTime.parse(row['timestamp'].toString()),
            row['conversationId'].toString(),
            row['message'].toString(),
          ),
        );
      }
    } on PlatformException {
      _logger.d('Error executing query');
    }
    return returnValue.reversed.toList();
  }

  @override
  Future<void> insertTextMessage(TextMessage message) async {
    if (!validMessage(message)) {
      _logger
          .d('Error inserting message in database, message object is invalid');
      throw const FormatException(
        'Error inserting message in database, '
        'message object is invalid.',
      );
    }
    final MutableDocument messageDocument = MutableDocument();
    messageDocument
        ?.setString('sender', message.sender)
        ?.setString('timestamp', message.timestamp.toString())
        ?.setString('conversationId', message.conversationId)
        ?.setString('message', message.message);
    try {
      if (await _database.saveDocument(messageDocument) == false) {
        _logger.d('Data not saved');
        // TODO: Data not saved
      }
    } catch (exception) {
      _logger.d('Error saving message in database');
      rethrow;
    }
  }

  bool validMessage(TextMessage message) {
    bool validMessage = true;
    final Map<String, dynamic> map = <String, dynamic>{
      'conversationId': message.conversationId,
      'timestamp': message.timestamp,
      'message': message.message
    };
    map.forEach((String key, dynamic value) {
      if (<dynamic>['', null, 0].contains(value)) {
        validMessage = false;
      }
    });
    return validMessage;
  }
}
