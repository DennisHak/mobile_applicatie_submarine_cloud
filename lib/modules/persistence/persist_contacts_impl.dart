import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/persistence/persist_contacts.dart';
import 'package:couchbase_lite/couchbase_lite.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

class PersistContactsImpl implements PersistContacts {
  final Logger _logger = Logger();

  @override
  Future<void> insertContact(Contact contact) {
    // TODO: implement insertContact
    throw UnimplementedError();
  }

  @override
  Future<void> updateContact(Contact contact) {
    // TODO: implement updateContact
    throw UnimplementedError();
  }

  @override
  Future<void> deleteContact(Contact contact) {
    // TODO: implement deleteContact
    throw UnimplementedError();
  }

  @override
  Future<Contact> getContact(int contactId) {
    // TODO: implement getContact
    throw UnimplementedError();
  }

  @override
  Future<List<Contact>> getContactList() {
    // TODO: implement getContactList
    throw UnimplementedError();
  }

  @override
  Future<Contact> getUserData() async {
    Contact returnValue;

    final Where query = QueryBuilder.select(<SelectResultAs>[
      SelectResult.property('alias'),
      SelectResult.property('publicKey')
    ]).from('submarine').where(
        Expression.property('username').equalTo(Expression.string('test')));

    try {
      final ResultSet results = await query.execute();
      _logger.i('Number of rows :: ${results.allResults().length}');

      for (final Result result in results) {
        final Map<String, dynamic> row = result.toMap();
        returnValue = Contact(row['alias'].toString(), null);
      }
    } on PlatformException catch (e) {
      _logger.d('Error executing query; $e');
    }

    return returnValue;
  }
}
