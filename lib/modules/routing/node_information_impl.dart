import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/node.dart';
import 'package:asd/modules/shared/routing/node_information.dart';

class NodeInformationImpl implements NodeInformation {
  @override
  Future<List<Node>> getNodeInfo() async {
    final List<Node> nodes = <Node>[];
    nodes.add(create('asd-p1-server1.asd.icaprojecten.nl',
        '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q='));
    nodes.add(create('asd-p1-server2.asd.icaprojecten.nl',
        '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q='));
    nodes.add(create('asd-p1-server3.asd.icaprojecten.nl',
        '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q='));
    return nodes;
  }

  @override
  Node create(String hostname, String key) {
    return Node(DestinationRelay(hostname, 25010), key);
  }
}
