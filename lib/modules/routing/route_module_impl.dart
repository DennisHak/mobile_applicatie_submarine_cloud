import 'dart:math';

import 'package:asd/modules/shared/encryption/encryption_module.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/node.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/path.dart';
import 'package:asd/modules/shared/routing/node_information.dart';
import 'package:asd/modules/shared/routing/route_module.dart';
import 'package:injector/injector.dart';

const int kLengthOfRoute = 3;

class RouteModuleImpl implements RouteModule {
  final NodeInformation _nodeInformation =
      Injector.appInstance.getDependency<NodeInformation>();
  final EncryptionModule _encryptionModule =
      Injector.appInstance.getDependency<EncryptionModule>();

  @override
  Future<Path> calculateRoute(int lengthOfPath) async {
    final List<Node> nodes = await getAndValidateNodes(lengthOfPath);
    return generatePath(nodes, lengthOfPath);
  }

  @override
  Future<List<Path>> calculateRoutes(
      int numberOfRoutes, int lengthOfPath) async {
    final List<Node> nodes = await getAndValidateNodes(lengthOfPath);
    final List<Path> paths = <Path>[];

    for (int i = 0; i < numberOfRoutes - 1; i++) {
      paths.add(await generatePath(nodes, lengthOfPath));
    }

    return paths;
  }

  Future<List<Node>> getAndValidateNodes(int lengthOfPath) async {
    final List<Node> nodes = await getActiveNodes();
    if (lengthOfPath > nodes.length) {
      throw ArgumentError('Required length of path is longer'
          ' than size of active nodes');
    }
    return nodes;
  }

  Future<Path> generatePath(List<Node> nodes, int lengthOfPath) async {
    final Path _path = Path();
    final List<Node> copyOfNodes = List<Node>.from(nodes);
    for (int i = 0; i < lengthOfPath; i++) {
      final int randomIndex = Random.secure().nextInt(copyOfNodes.length);
      _path.addNodeToPath(copyOfNodes.removeAt(randomIndex));
    }

    return _path;
  }

  @override
  Future<Onion> makeOnion(Message message, Contact receiver) async {
    final Path route = await calculateRoute(kLengthOfRoute);
    return _encryptionModule.encryptOnion(route, message, receiver);
  }

  Future<List<Node>> getActiveNodes() {
    // TODO: implementie connection to node directory server instead of mock
    return _nodeInformation.getNodeInfo();
  }
}
