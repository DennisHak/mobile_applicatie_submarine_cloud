import 'package:asd/bloc/message/bloc.dart';
import 'package:asd/screens/chat_conversation.dart';
import 'package:asd/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute<dynamic>(
          builder: (_) => const Home(title: 'Confidential Messenger'),
        );
      case '/send-message':
        return _sendMessageRoute();
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _sendMessageRoute() {
    return MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => BlocProvider<MessageBloc>(
              create: (BuildContext context) => MessageBloc(),
              child: const ChatConversation(title: 'ASD-P 2019-2020 S2'),
            ));
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute<dynamic>(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
