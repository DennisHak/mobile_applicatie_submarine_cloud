import 'package:flutter/material.dart';

final ThemeData mainTheme = ThemeData.dark().copyWith(
  backgroundColor: const Color(0xFF46494C),
  primaryColor: const Color(0xFF1985A1),
  appBarTheme: const AppBarTheme(),
  textTheme: const TextTheme(
    bodyText1: TextStyle(
      fontFamily: 'Nunito',
      fontSize: 40.0,
    ),
  ),
);
