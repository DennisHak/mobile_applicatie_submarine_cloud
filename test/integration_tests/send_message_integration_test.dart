import 'dart:io';

import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/connection/database_connection.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/modules/encryption/asymmetric_encryption_impl.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/connection/send_message.dart';
import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/node.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persist_messages.dart';
import 'package:asd/modules/shared/routing/node_information.dart';
import 'package:asd/modules/shared/routing/route_module.dart';
import 'package:couchbase_lite/couchbase_lite.dart';
import 'package:flutter/services.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';
import 'package:pointycastle/export.dart';
import 'package:string_validator/string_validator.dart';
import 'package:test/test.dart';

class SendMessageMock extends Mock implements SendMessage {}

class ConnectionModuleMock extends Mock implements ConnectionModule {}

class DatabaseConnectionMock extends Mock implements DatabaseConnection {}

class PersistMessagesMock extends Mock implements PersistMessages {}

class DatabaseMock extends Mock implements Database {}

class NodeInformationMock extends Mock implements NodeInformation {}

class RouteModuleMock extends Mock implements RouteModule {}

void main() {
  final AsymmetricEncryption asymmetricEncryption = AsymmetricEncryptionImpl();
  final PublicKey publicKey =
      asymmetricEncryption.generateRandomKeyPair().publicKey;

  const String host = '127.0.0.1';
  const int port = 25010;
  const String key = '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q=';
  final TextMessage message =
      TextMessage('Test', DateTime.now(), 'ConvId1', 'Test Message');
  final Contact userData = Contact('receiver', publicKey);

  final Injector injector = Injector.appInstance;

  final Database databaseMock = DatabaseMock();

  void injectNodeInfoMock() {
    injector.removeByKey<NodeInformation>();
    injector.registerSingleton<NodeInformation>((_) => NodeInformationMock());
  }

  void injectDatabaseConnectionMock() {
    injector.removeByKey<DatabaseConnection>();
    final DatabaseConnection databaseConnection = DatabaseConnectionMock();
    when(databaseConnection.database).thenReturn(databaseMock);
    injector.registerSingleton<DatabaseConnection>(
      (_) => databaseConnection,
    );
  }

  void injectSendMessageMock() {
    injector.removeByKey<SendMessage>();
    injector.registerSingleton<SendMessage>((_) => SendMessageMock());
  }

  void injectConnectionModule() {
    injector.removeByKey<ConnectionModule>();
    injector.registerSingleton<ConnectionModule>((_) => ConnectionModuleMock());
  }

  void injectRouteModuleMock() {
    injector.removeByKey<RouteModule>();
    injector.registerSingleton<RouteModule>((_) => RouteModuleMock());
  }

  void injectPersistMessagesMock() {
    injector.removeByKey<PersistMessages>();
    injector.registerSingleton<PersistMessages>((_) => PersistMessagesMock());
  }

  setUp(() {
    injectorBootstrap();
  });

  tearDown(() => injector.clearAll());

  ControllerModule getController() {
    final ControllerModule controllerModule =
        injector.getDependency<ControllerModule>();
    controllerModule.getDependencies();
    return controllerModule;
  }

  test('Send message succesfully', () async {
    // Inject Mocks
    injectNodeInfoMock();
    injectPersistMessagesMock();
    injectSendMessageMock();

    final NodeInformation nodeInformation =
        injector.getDependency<NodeInformation>();

    when(nodeInformation.getNodeInfo()).thenAnswer((Invocation realInvocation) {
      final List<Node> nodes = <Node>[];
      for (int i = 0; i < 3; i++) {
        nodes.add(Node(DestinationRelay(host, port), key));
      }
      return Future<List<Node>>.delayed(
          const Duration(milliseconds: 50), () => nodes);
    });

    final SendMessage sendMessage = injector.getDependency<SendMessage>();
    final PersistMessages persistMessages =
        injector.getDependency<PersistMessages>();

    final ControllerModule controllerModule = getController();
    await controllerModule.sendMessage(message, userData);
    verify(persistMessages.insertTextMessage(any)).called(1);

    final dynamic callParams =
        verify(sendMessage.sendMessage(captureAny, captureAny)).captured;

    expect(callParams[0] is Socket, true);
    if (callParams[0] is Socket) {
      final Socket socket = callParams[0] as Socket;
      expect(socket != null, true);
      expect(socket.address.host, host);
      expect(socket.remotePort, port);
      socket.close();
    }

    expect(callParams[1] is Onion, true);
    if (callParams[1] is Onion) {
      final Onion onion = callParams[1] as Onion;
      expect(onion.command, 'RELAY');
      expect(isBase64(onion.data), true);
    }
  });

  test('Send message with invalid message object', () async {
    // Inject Mocks
    injectConnectionModule();
    injectRouteModuleMock();

    final ControllerModule controllerModule = getController();
    const Message message = TextMessage(null, null, null, null);

    expect(() => controllerModule.sendMessage(message, userData),
        throwsA(isA<Exception>()));
  });

  test('Send message, exception on inserting message into database', () async {
    injectDatabaseConnectionMock();
    injectConnectionModule();
    injectRouteModuleMock();

    final ControllerModule controllerModule = getController();

    when(databaseMock.saveDocument(any))
        .thenAnswer((Invocation realInvocation) {
      return Future<bool>.delayed(
        const Duration(),
        () => throw PlatformException(code: 'test'),
      );
    });

    expect(() => controllerModule.sendMessage(message, userData),
        throwsA(isA<Exception>()));
  });

  test('Send message, exception on making the onion', () async {
    injectDatabaseConnectionMock();
    injectRouteModuleMock();

    final ControllerModule controllerModule = getController();
    final RouteModule routeModule = injector.getDependency<RouteModule>();

    when(routeModule.makeOnion(any, any))
        .thenAnswer((Invocation realInvocation) {
      return Future<Onion>.delayed(const Duration(), () => throw Exception());
    });

    expect(() => controllerModule.sendMessage(message, userData),
        throwsA(isA<Exception>()));
  });

  test('Send message, exception on making the onion', () async {
    injectDatabaseConnectionMock();
    injectRouteModuleMock();

    final ControllerModule controllerModule = getController();
    final RouteModule routeModule = injector.getDependency<RouteModule>();

    when(routeModule.makeOnion(any, any))
        .thenAnswer((Invocation realInvocation) {
      return Future<Onion>.delayed(const Duration(), () => throw Exception());
    });

    expect(() => controllerModule.sendMessage(message, userData),
        throwsA(isA<Exception>()));
  });

  test('Send message, invalid destination in onion', () async {
    injectDatabaseConnectionMock();
    injectRouteModuleMock();

    final ControllerModule controllerModule = getController();
    final RouteModule routeModule = injector.getDependency<RouteModule>();

    when(routeModule.makeOnion(any, any))
        .thenAnswer((Invocation realInvocation) {
      return Future<Onion>.delayed(const Duration(), () {
        return Onion(
          DestinationRelay(null, null),
          'RELAY',
          'test',
        );
      });
    });

    expect(() => controllerModule.sendMessage(message, userData),
        throwsA(isA<Exception>()));
  });
}
