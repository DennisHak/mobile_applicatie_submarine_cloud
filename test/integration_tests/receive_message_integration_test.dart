import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/message/message_handler.dart';
import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persist_messages.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class PersistMessagesMock extends Mock implements PersistMessages {}

void main() {
  const String base64EncodedKey =
      'BzsTmoqqOUHVXC9gT3t2vrkVG+p8OeTwC2njtM6IgecV5rRAsXOvn16CrMPS/eIuNOX2yi8DFmIlrXUIumV2YzO5SouusX+RdVHsjLow/MOy8e/KiZ56kijIoJhSPWm4v3Q789c8GVDYr8H7J9qsGrZ3/DVmIhFPElR1ROsVRtz86l9T79diQ6x6IOT9jl6udEzyvRPq+i4k0XPRw5vqOWDG2uORjKWvF102/2O67bigvvTUTVOivSps+/oyqLEMPB0u4eAskA3vXuZfNMkpHqLBojT+DTXW+MArkBGaTq9jf97w+j1nUXxFGIlVvh5xfIyIG3Gapo0ly93QQITH2g==';

  final TextMessage _message =
      TextMessage('Test', DateTime.now(), 'ConvId1', 'Test Message');
  final DestinationClient destination = DestinationClient(
    'Test',
    base64EncodedKey,
  );
  final Injector injector = Injector.appInstance;
  final Onion onion = Onion(destination, 'RELAY', _message.message);

  setUp(() => injectorBootstrap());
  tearDown(() => injector.clearAll());

  test('Receive message succesfully and insert into db', () async {
    injector.removeByKey<PersistMessages>();
    injector.registerSingleton<PersistMessages>((_) => PersistMessagesMock());

    final PersistMessages persistMessages =
        injector.getDependency<PersistMessages>();
    final ConnectionModule connectionModule =
        injector.getDependency<ConnectionModule>();
    final ControllerModule controllerModule =
        injector.getDependency<ControllerModule>();
    controllerModule.getDependencies();

    (connectionModule as MessageHandler).incomingMessage(onion);
    verify(persistMessages.insertTextMessage(any)).called(1);
  });
}
