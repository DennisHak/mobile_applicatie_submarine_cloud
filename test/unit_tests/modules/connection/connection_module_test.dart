import 'dart:io';

import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/connection/send_message.dart';
import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';

class SendMessageMock extends Mock implements SendMessage {}

void main() {
  final Injector injector = Injector.appInstance;

  setUp(() {
    injectorBootstrap();
    injector.removeByKey<SendMessage>();
    injector.registerSingleton<SendMessage>((_) => SendMessageMock());
  });
  tearDown(() => injector.clearAll());

  test('test send message with socket with valid destination', () async {
    final SendMessage sendMessage =
        Injector.appInstance.getDependency<SendMessage>();

    final ConnectionModule connectionModule =
        Injector.appInstance.getDependency<ConnectionModule>();

    await connectionModule.connect('127.0.0.1', 25010);
    await connectionModule.sendMessage(
      Onion(
        DestinationRelay('127.0.0.1', 25010),
        'RELAY',
        'test',
      ),
    );
    verify(sendMessage.sendMessage(any, any)).called(1);
  });

  test('test send message with invalid destination', () async {
    final ConnectionModule connectionModule =
        Injector.appInstance.getDependency<ConnectionModule>();

    await connectionModule.connect('127.0.0.1', 25010);

    expect(
      () => connectionModule.sendMessage(
        Onion(
          DestinationClient(null, null),
          'RELAY',
          'test',
        ),
      ),
      throwsA(
        isA<FormatException>(),
      ),
    );
  });

  test('test send message with invalid destination 2', () async {
    final ConnectionModule connectionModule =
        Injector.appInstance.getDependency<ConnectionModule>();

    await connectionModule.connect('127.0.0.1', 25010);

    expect(
      () => connectionModule.sendMessage(
        Onion(
          DestinationRelay(null, null),
          'RELAY',
          'test',
        ),
      ),
      throwsA(
        isA<FormatException>(),
      ),
    );
  });

  test('test send message with empty onion', () async {
    final ConnectionModule connectionModule =
        Injector.appInstance.getDependency<ConnectionModule>();

    expect(
      () => connectionModule.sendMessage(null),
      throwsA(
        isA<FormatException>(),
      ),
    );
  });

  test('test successful connection to socket', () async {
    final ConnectionModule connectionModule =
        Injector.appInstance.getDependency<ConnectionModule>();
    await connectionModule.connect('127.0.0.1', 25010);
  });

  test('test unsuccessful connection to socket', () async {
    final ConnectionModule connectionModule =
        Injector.appInstance.getDependency<ConnectionModule>();
    expect(
        () async =>
            connectionModule.connect('this.domain.does.not.exist.org', 25010),
        throwsA(
          isA<SocketException>(),
        ));
  });
}
