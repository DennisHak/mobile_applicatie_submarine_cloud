import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/connection/socket_server_impl.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/message/message_handler.dart';
import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class MockConnectionModule extends Mock
    implements ConnectionModule, MessageHandler {}

class MockServerSocket extends Mock implements ServerSocket {}

class MockSocket extends Mock implements Socket {}

const String host = '127.0.0.1';
const int port = 25010;
const String command = 'RELAY';
const String testMessage = 'Test message';
const int delay = 50;
const String onErrorNameArg = 'onError';
const String onDoneNameArg = 'onDone';
const String base64EncodedKey =
    'BzsTmoqqOUHVXC9gT3t2vrkVG+p8OeTwC2njtM6IgecV5rRAsXOvn16CrMPS/eIuNOX2yi8DFmIlrXUIumV2YzO5SouusX+RdVHsjLow/MOy8e/KiZ56kijIoJhSPWm4v3Q789c8GVDYr8H7J9qsGrZ3/DVmIhFPElR1ROsVRtz86l9T79diQ6x6IOT9jl6udEzyvRPq+i4k0XPRw5vqOWDG2uORjKWvF102/2O67bigvvTUTVOivSps+/oyqLEMPB0u4eAskA3vXuZfNMkpHqLBojT+DTXW+MArkBGaTq9jf97w+j1nUXxFGIlVvh5xfIyIG3Gapo0ly93QQITH2g==';

void main() {
  final Injector injector = Injector.appInstance;
  ServerSocket serverSocket;
  Socket clientConnection;

  setUp(() {
    serverSocket = MockServerSocket();
    clientConnection = MockSocket();
    injectorBootstrap();
    injector.removeByKey<ConnectionModule>();
    injector.registerSingleton<ConnectionModule>((_) => MockConnectionModule());
  });

  tearDown(() => injector.clearAll());

  void mockSocketServerListener() {
    when(
      serverSocket.listen(any),
    ).thenAnswer((Invocation invocation) {
      final Function(Socket) handler =
          invocation.positionalArguments[0] as Function(Socket);
      return Stream<Socket>.fromFuture(
        Future<Socket>.delayed(
          const Duration(milliseconds: delay),
          () => clientConnection,
        ),
      ).listen(handler);
    });
  }

  void mockSocketListener(String message) {
    when(clientConnection.listen(
      any,
      onDone: anyNamed(onDoneNameArg),
      onError: anyNamed(onErrorNameArg),
    )).thenAnswer((Invocation invocation) {
      final Map<Symbol, dynamic> namedArgs = invocation.namedArguments;
      final Function(Uint8List onData) onData =
          invocation.positionalArguments[0] as Function(Uint8List onData);
      final Function(dynamic) onError =
          namedArgs[const Symbol(onErrorNameArg)] as Function(dynamic);
      final Function() onDone =
          namedArgs[const Symbol(onDoneNameArg)] as Function();

      final Uint8List bytes = Uint8List.fromList(message.codeUnits);

      return Stream<Uint8List>.fromFuture(
        Future<Uint8List>.delayed(
          const Duration(milliseconds: delay),
          () => bytes,
        ),
      ).listen(onData, onError: onError, onDone: onDone);
    });
  }

  test('Succesfully handle incoming message', () async {
    final Onion onion = Onion(
      DestinationClient('alias', base64EncodedKey),
      'RELAY',
      testMessage,
    );
    final Map<String, dynamic> json = onion.toJson();
    final String message = jsonEncode(json);
    mockSocketServerListener();
    mockSocketListener(message);

    SocketServerHandler(serverSocket);
    final MessageHandler messageHandler =
        injector.getDependency<ConnectionModule>() as MessageHandler;

    await untilCalled(messageHandler.incomingMessage(any));

    final List<dynamic> callParams =
        verify(messageHandler.incomingMessage(captureAny)).captured;

    expect(callParams.length, 1);
    expect(callParams[0] is Onion, true);

    if (callParams[0] is Onion) {
      final Onion onion = callParams[0] as Onion;
      expect(onion.command, 'RELAY');
      expect(onion.data, testMessage);
      expect(onion.destination is DestinationClient, true);
      expect((onion.destination as DestinationClient).alias, 'alias');
    }
  });

  test('Handle incoming message with incorrect format', () async {
    mockSocketServerListener();
    mockSocketListener('Wrong message format');

    SocketServerHandler(serverSocket);
    final MessageHandler messageHandler =
        injector.getDependency<ConnectionModule>() as MessageHandler;

    await Future<dynamic>.delayed(const Duration(milliseconds: delay * 4));
    verifyNever(messageHandler.incomingMessage(any));
  });

  test('Shutdown socket server and close client connections', () async {
    mockSocketServerListener();
    mockSocketListener('');

    final SocketServerHandler handler = SocketServerHandler(serverSocket);
    handler.stopServer();

    verify(serverSocket.close()).called(1);
    await untilCalled(clientConnection.close());
    verify(clientConnection.close()).called(1);
    verify(clientConnection.destroy()).called(1);
  });
}
