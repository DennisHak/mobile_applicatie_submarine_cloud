import 'dart:io';

import 'package:asd/modules/connection/send_message_impl.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class SocketMock extends Mock implements Socket {}

void main() {
  test('send message success', () async {
    final Socket socket = SocketMock();

    final SendMessageImpl sendMessageImpl = SendMessageImpl();
    sendMessageImpl.sendMessage(
        socket,
        Onion(
          DestinationRelay(null, null),
          'RELAY',
          'test',
        ));

    verify(socket.write(any)).called(1);
    verify(socket.flush()).called(1);
  });

  test('send message socket throws exception', () async {
    final Socket socket = SocketMock();
    when(socket.write(any)).thenThrow(const SocketException('message'));

    final SendMessageImpl sendMessageImpl = SendMessageImpl();
    expect(
      () => sendMessageImpl.sendMessage(
          socket,
          Onion(
            DestinationRelay(null, null),
            'RELAY',
            'test',
          )),
      throwsA(
        isA<IOException>(),
      ),
    );
    verify(socket.close()).called(1);
  });
}
