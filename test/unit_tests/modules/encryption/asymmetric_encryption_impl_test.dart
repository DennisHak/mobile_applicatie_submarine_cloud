import 'dart:typed_data';
import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/encryption/asymmetric_encryption_impl.dart';
import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:pointycastle/export.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';

class AsymmetricEncryptionMock extends Mock implements AsymmetricEncryption {}

void main() {
  final Injector injector = Injector.appInstance;
  AsymmetricEncryption _sut;
  const String testMessage = 'test message';

  setUp(() {
    injectorBootstrap();
    _sut = AsymmetricEncryptionImpl();
  });

  tearDown(() => injector.clearAll());

  test('Key pair generation is randomized', () {
    //act
    final AsymmetricKeyPair<PublicKey, PrivateKey> kp1 =
        _sut.generateRandomKeyPair();
    final AsymmetricKeyPair<PublicKey, PrivateKey> kp2 =
        _sut.generateRandomKeyPair();
    //assert
    assert(kp1.publicKey.hashCode != kp2.publicKey.hashCode &&
        kp1.privateKey.hashCode != kp2.privateKey.hashCode);
  });

  test('String message gets encrypted', () {
    //arrange
    final AsymmetricKeyPair<PublicKey, PrivateKey> kp1 =
        _sut.generateRandomKeyPair();
    //act
    final Uint8List uint8list = _sut.encryptAsymmetric(
        Uint8List.fromList(testMessage.codeUnits), kp1.publicKey);
    //assert
    assert(String.fromCharCodes(uint8list) != testMessage);
  });

  test('Public key is null', () {
    expect(
        () => _sut.encryptAsymmetric(
            Uint8List.fromList(testMessage.codeUnits), null),
        throwsA(isInstanceOf<StateError>()));
  });

  test('Private key is null', () {
    //arrange
    final AsymmetricKeyPair<PublicKey, PrivateKey> kp1 =
        _sut.generateRandomKeyPair();
    final Uint8List encryptedMessage = _sut.encryptAsymmetric(
        Uint8List.fromList(testMessage.codeUnits), kp1.publicKey);
    //act & assert
    expect(
        () =>
            _sut.decryptAsymmetric(Uint8List.fromList(encryptedMessage), null),
        throwsA(isInstanceOf<StateError>()));
  });

  test('Message remains the same after decryption', () {
    //arrange
    final AsymmetricKeyPair<PublicKey, PrivateKey> kp1 =
        _sut.generateRandomKeyPair();
    final Uint8List encryptedMessage = _sut.encryptAsymmetric(
        Uint8List.fromList(testMessage.codeUnits), kp1.publicKey);
    //act
    final Uint8List decryptedMessage = _sut.decryptAsymmetric(
        Uint8List.fromList(encryptedMessage), kp1.privateKey);
    //assert
    expect(String.fromCharCodes(decryptedMessage), testMessage);
  });
}
