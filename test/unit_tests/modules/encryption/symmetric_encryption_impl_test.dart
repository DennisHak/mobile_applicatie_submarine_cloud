import 'dart:convert';
import 'dart:typed_data';

import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/encryption/symmetric_encryption_impl.dart';
import 'package:asd/modules/shared/encryption/asymmetric_encryption.dart';
import 'package:asd/modules/shared/encryption/symmetric_encryption.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/node.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/models/path.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:encrypt/encrypt.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';
import 'package:pointycastle/export.dart';
import 'package:test/test.dart';

class SymmetricEncryptionMock extends Mock implements SymmetricEncryption {}

void main() {
  injectorBootstrap();
  final Injector injector = Injector.appInstance;
  final SymmetricEncryption symmetricEncryption =
      injector.getDependency<SymmetricEncryption>();
  final SymmetricEncryption _sut = SymmetricEncryptionImpl();

  final AsymmetricEncryption asymmetricEncryption =
      injector.getDependency<AsymmetricEncryption>();
  final PublicKey publicKey =
      asymmetricEncryption.generateRandomKeyPair().publicKey;

  final Key testSymmetricKey =
      Key.fromBase64('9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q=');

  final String base64EncodedKey = const Base64Encoder().convert(
    asymmetricEncryption.encryptAsymmetric(
      testSymmetricKey.bytes,
      publicKey,
    ),
  );

  final Contact testReceiver = Contact('ALIAS_OF_RECEIVER', publicKey);

  final Message testMessage = TextMessage(
    testReceiver.alias,
    DateTime.parse('2018-05-28 13:18:00z'),
    'conversationId',
    'test message',
  );

  final Onion testOnion = Onion(
    DestinationClient('ALIAS', base64EncodedKey),
    'RELAY',
    symmetricEncryption.encryptSymmetric(
      jsonEncode(testMessage),
      testSymmetricKey,
    ),
  );

  test('Create onion with three dedicated layers', () {
    final Path path = Path();
    path.addNodeToPath(Node(
        DestinationRelay(
          'ENTRY_NODE',
          1,
        ),
        '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q='));
    path.addNodeToPath(Node(
        DestinationRelay(
          'RELAY_NODE',
          2,
        ),
        '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q='));
    path.addNodeToPath(Node(DestinationRelay('EXIT_NODE', 3),
        '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q='));

    final Onion onion =
        symmetricEncryption.encryptOnion(path, testMessage, testReceiver);

    expect((onion.destination as DestinationRelay).hostname, 'ENTRY_NODE');
    expect((onion.destination as DestinationRelay).port, 1);

    final Onion onionWithoutEntryNode =
        createTestOnion(path, onion, symmetricEncryption);

    expect((onionWithoutEntryNode.destination as DestinationRelay).hostname,
        'RELAY_NODE');
    expect((onionWithoutEntryNode.destination as DestinationRelay).port, 2);

    final Onion onionWithoutRelayNode =
        createTestOnion(path, onionWithoutEntryNode, symmetricEncryption);

    expect((onionWithoutRelayNode.destination as DestinationRelay).hostname,
        'EXIT_NODE');
    expect((onionWithoutRelayNode.destination as DestinationRelay).port, 3);

    final Onion onionWithoutExitNode =
        createTestOnion(path, onionWithoutRelayNode, symmetricEncryption);

    expect((onionWithoutExitNode.destination as DestinationClient).alias,
        'ALIAS_OF_RECEIVER');
  });

  test('Create onion with many layers', () {
    const int nodeAmount = 7;
    final Path path = Path();
    for (int i = 0; i < nodeAmount; i++) {
      path.addNodeToPath(Node(DestinationRelay(String.fromCharCode(i), 0),
          '9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q='));
    }

    Onion onion =
        symmetricEncryption.encryptOnion(path, testMessage, testReceiver);

    for (int i = 0; i < nodeAmount; i++) {
      expect((onion.destination as DestinationRelay).hostname,
          path.getNodes()[i].destination.hostname);
      onion = createTestOnion(path, onion, symmetricEncryption);
    }

    expect((onion.destination as DestinationClient).alias, 'ALIAS_OF_RECEIVER');
  });

  test(
      'Create onion requires more than one node to function, '
      'otherwise an IllegalArgumentException is thrown', () {
    final Path path = Path();
    path.addNodeToPath(Node(DestinationRelay('lone node', 0),
        '/E3kzSjyiOp3gS7OU5qW8dRvK3jliQyhcMUj/pzQeLM='));
    expect(
        () => symmetricEncryption.encryptOnion(
              path,
              testMessage,
              testReceiver,
            ),
        throwsArgumentError);
  });

  test('Generated key is not null', () async {
    final Key testKey = _sut.generateRandomSymmetricKey();
    expect(testKey, isNotNull);
  });

  test('Symmetric decryption: No content to decrypt given', () {
    expect(() => _sut.decryptSymmetric(null, testSymmetricKey),
        throwsArgumentError);
  });

  test('Symmetric encryption: No content to encrypt given', () {
    expect(() => _sut.encryptSymmetric(null, testSymmetricKey),
        throwsArgumentError);
  });

  test('Symmetric decryption: No secretKey given', () {
    expect(
        () => _sut.decryptSymmetric('test message', null), throwsArgumentError);
  });

  test('Symmetric encryption: No secretKey given', () {
    expect(() => _sut.encryptSymmetric(testOnion, null), throwsArgumentError);
  });

  test('Symmetric encryption: Content to encrypt is not an onion or a string',
      () {
    expect(() => _sut.encryptSymmetric(1337, testSymmetricKey),
        throwsArgumentError);
  });
}

Onion createTestOnion(
    Path path, Onion previousOnion, SymmetricEncryption symmetricEncryption) {
  final Uint8List dataOfOnion = symmetricEncryption.decryptSymmetric(
    previousOnion.data,
    Key.fromBase64(path.getNodes()[0].key),
  );
  final String entryNodeInJsonString = const Utf8Decoder().convert(dataOfOnion);
  final Map<String, dynamic> nodeInJson =
      jsonDecode(entryNodeInJsonString) as Map<String, dynamic>;
  return Onion.fromJson(nodeInJson);
}
