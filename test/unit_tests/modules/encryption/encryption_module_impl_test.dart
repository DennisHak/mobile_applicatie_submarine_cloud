import 'dart:convert';
import 'dart:typed_data';

import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/encryption/encryption_module_impl.dart';
import 'package:asd/modules/shared/encryption/encryption_module.dart';
import 'package:asd/modules/shared/encryption/symmetric_encryption.dart';
import 'package:asd/modules/shared/models/destination_client.dart';
import 'package:asd/modules/shared/models/onion.dart';
import 'package:asd/modules/shared/routing/route_module.dart';
import 'package:encrypt/encrypt.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class RouteModuleMock extends Mock implements RouteModule {}

class SymmetricEncryptionMock extends Mock implements SymmetricEncryption {}

class EncryptionModuleMock extends Mock implements EncryptionModule {}

void main() {
  final Key testSymmetricKey =
      Key.fromBase64('9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q=');
  final Key secondTestKey =
      Key.fromBase64('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=');

  final Onion onionBeforeEncryption =
      Onion(DestinationClient('alias', null), null, 'Test message');

  const String encryptedOnion = 'This is an encrypted onion';
  final Uint8List decryptedOnion =
      const Utf8Encoder().convert('This is an decrypted onion');

  SymmetricEncryption _symmetricEncryption;

  EncryptionModule _sut;

  final Injector injector = Injector.appInstance;

  setUp(() {
    injectorBootstrap();

    injector.removeByKey<SymmetricEncryption>();
    injector.registerSingleton<SymmetricEncryption>(
        (_) => SymmetricEncryptionMock());
    _symmetricEncryption = injector.getDependency<SymmetricEncryption>();

    _sut = EncryptionModuleImpl();
  });

  tearDown(() => injector.clearAll());

  test('Symmetric encryption success', () async {
    when(_symmetricEncryption.encryptSymmetric(
            onionBeforeEncryption, testSymmetricKey))
        .thenReturn(encryptedOnion);

    final String contentToEncrypt =
        _sut.encryptSymmetric(onionBeforeEncryption, testSymmetricKey);

    expect(contentToEncrypt, encryptedOnion);
  });

  test('Symmetric encryption failure wrong key', () async {
    when(_symmetricEncryption.encryptSymmetric(
            onionBeforeEncryption, testSymmetricKey))
        .thenReturn(encryptedOnion);

    final String contentToEncrypt =
        _sut.encryptSymmetric(onionBeforeEncryption, secondTestKey);

    expect(contentToEncrypt, null);
  });

  test('Symmetric decryption success', () async {
    final Onion onionWithoutExitNode = Onion(
      DestinationClient('alias', null),
      null,
      encryptedOnion,
    );

    when(_symmetricEncryption.decryptSymmetric(
            onionWithoutExitNode.data, testSymmetricKey))
        .thenReturn(decryptedOnion);

    final Uint8List decryptedResult =
        _sut.decryptSymmetric(onionWithoutExitNode.data, testSymmetricKey);

    expect(decryptedResult, decryptedOnion);
  });

  test('Symmetric decryption failure wrong key', () async {
    final Onion onionWithoutExitNode =
        Onion(DestinationClient('alias', null), null, encryptedOnion);
    when(_symmetricEncryption.decryptSymmetric(
            onionWithoutExitNode.data, testSymmetricKey))
        .thenReturn(decryptedOnion);
    expect(
      _sut.decryptSymmetric(onionWithoutExitNode.data, secondTestKey),
      null,
    );
  });
}
