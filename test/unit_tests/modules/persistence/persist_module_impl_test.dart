import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/modules/shared/connection/connection_module.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persistence_module.dart';
import 'package:asd/modules/shared/routing/route_module.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';

class PersistenceModuleMock extends Mock implements PersistenceModule {}

class RouteModuleMock extends Mock implements RouteModule {}

class ConnectionModuleMock extends Mock implements ConnectionModule {}

void main() {
  final Injector injector = Injector.appInstance;

  setUp(() {
    injectorBootstrap();
    injector.removeByKey<PersistenceModule>();
    injector.removeByKey<RouteModule>();
    injector.removeByKey<ConnectionModule>();
    injector
        .registerSingleton<PersistenceModule>((_) => PersistenceModuleMock());
    injector.registerSingleton<RouteModule>((_) => RouteModuleMock());
    injector.registerSingleton<ConnectionModule>((_) => ConnectionModuleMock());
  });

  tearDown(() => injector.clearAll());

  ControllerModule getController() {
    final ControllerModule controllerModule =
        injector.getDependency<ControllerModule>();
    return controllerModule;
  }

  group('Initialise database', () {
    test('check if function calls function on persist message', () async {
      final PersistenceModule _persistenceModule =
          injector.getDependency<PersistenceModule>();

      final ControllerModule controllerModule = getController();
      controllerModule.getDependencies();

      const Contact receiver = Contact('receiver', null);

      final TextMessage textMessage = TextMessage(
        'test',
        DateTime.now(),
        'test',
        'test',
      );

      await controllerModule.sendMessage(textMessage, receiver);
      verify(_persistenceModule.insertMessage(any)).called(1);
    });
  });
}
