import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/connection/database_connection.dart';
import 'package:asd/modules/persistence/persist_messages_impl.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:couchbase_lite/couchbase_lite.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';

class DatabaseConnectionMock extends Mock implements DatabaseConnection {}

class DatabaseMock extends Mock implements Database {}

class MutableDocumentMock extends Mock implements MutableDocument {}

void main() {
  final Injector injector = Injector.appInstance;
  final DatabaseConnectionMock dbConnectionMock = DatabaseConnectionMock();
  final DatabaseMock dbMock = DatabaseMock();
  final MutableDocumentMock documentMock = MutableDocumentMock();

  setUp(() {
    injectorBootstrap();
    injector.removeByKey<DatabaseConnection>();
    injector.registerSingleton<DatabaseConnection>((_) => dbConnectionMock);

    when(dbConnectionMock.database).thenReturn(dbMock);
    when(dbConnectionMock.message).thenReturn(documentMock);
  });
  tearDown(() => injector.clearAll());

  group('insert message', () {
    test('check if message is inserted correctly', () async {
      final PersistMessagesImpl persistMessagesImpl = PersistMessagesImpl();

      final TextMessage textMessage =
          TextMessage('Mark', DateTime.now(), '1', 'Hello World!');

      await persistMessagesImpl.insertTextMessage(textMessage);

      when(dbMock.saveDocument(any)).thenAnswer((Invocation realInvocation) {
        return Future<bool>.delayed(const Duration(), () => true);
      });

      verify(dbMock.saveDocument(any)).called(1);
    });

    test('check if message validation works correctly', () async {
      final PersistMessagesImpl persistMessagesImpl = PersistMessagesImpl();

      TextMessage textMessage = const TextMessage(
        'alias',
        null,
        '1',
        'Hello World!',
      );
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = const TextMessage(null, null, '1', 'Hello World!');
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = const TextMessage('alias', null, '1', 'Hello World!');
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = TextMessage(null, DateTime.now(), null, 'Hello World!');
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = TextMessage('alias', DateTime.now(), null, 'Hello World!');
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = TextMessage(null, DateTime.now(), '1', null);
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = TextMessage('alias', DateTime.now(), '1', null);
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = const TextMessage(null, null, null, 'Hello World!');
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = const TextMessage('alias', null, null, 'Hello World!');
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = TextMessage(null, DateTime.now(), null, null);
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = TextMessage('alias', DateTime.now(), null, null);
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = const TextMessage(null, null, null, null);
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));

      textMessage = const TextMessage('alias', null, null, null);
      expect(
          () => persistMessagesImpl.insertTextMessage(textMessage),
          throwsA(
            isA<Exception>(),
          ));
    });
  });
}
