import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/routing/node_information_impl.dart';
import 'package:asd/modules/routing/route_module_impl.dart';
import 'package:asd/modules/shared/models/destination_relay.dart';
import 'package:asd/modules/shared/models/node.dart';
import 'package:asd/modules/shared/models/path.dart';
import 'package:asd/modules/shared/routing/node_information.dart';
import 'package:asd/modules/shared/routing/route_module.dart';
import 'package:flutter/material.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class RouteModuleImplTest extends Mock implements RouteModule {}

void main() {
  final Injector injector = Injector.appInstance;
  setUp(() => injectorBootstrap());
  tearDown(() => injector.clearAll());

  group('routeModuleImplementation', () {
    test('Generate path test', () async {
      final RouteModuleImpl _routeModule = RouteModuleImpl();
      final List<Node> nodes = <Node>[];
      const int lengthOfPath = 3;

      nodes.add(Node(
          DestinationRelay('asd-p1-server1.asd.icaprojecten.nl', 25010),
          'mockKey01'));
      nodes.add(Node(
          DestinationRelay('asd-p1-server2.asd.icaprojecten.nl', 25010),
          'mockKey02'));
      nodes.add(Node(
          DestinationRelay('asd-p1-server3.asd.icaprojecten.nl', 25010),
          'mockKey03'));

      final Path path = await _routeModule.generatePath(nodes, lengthOfPath);
      expect(path.getNodes().length, nodes.length);
    });
  });

  //Deze tests alleen uitvoeren als de NodeRoutingServer beschikbaar is.
  group('routeModuleImplementationServer', () {
    test('Calculate route', () async {
      final RouteModuleImpl _routeModule = RouteModuleImpl();

      final List<Node> nodes = <Node>[];
      const int lengthOfPath = 3;
      final NodeInformation nodeInformation = NodeInformationImpl();
      final List<Node> nodesFromServer = await nodeInformation.getNodeInfo();

      nodes.add(Node(
          DestinationRelay('asd-p1-server1.asd.icaprojecten.nl', 25010),
          'mockKey01'));
      nodes.add(Node(
          DestinationRelay('asd-p1-server2.asd.icaprojecten.nl', 25010),
          'mockKey02'));
      nodes.add(Node(
          DestinationRelay('asd-p1-server3.asd.icaprojecten.nl', 25010),
          'mockKey03'));

      final Path path = await _routeModule.calculateRoute(lengthOfPath);

      expect(path.getNodes().length, nodesFromServer.length);
    });

    test('Validate nodes', () async {
      final RouteModuleImpl _routeModule = RouteModuleImpl();
      //arrange
      final List<Node> nodes = <Node>[];
      nodes.add(Node(
          DestinationRelay('asd-p1-server1.asd.icaprojecten.nl', 25010),
          'mockKey01'));
      nodes.add(Node(
          DestinationRelay('asd-p1-server2.asd.icaprojecten.nl', 25010),
          'mockKey02'));
      nodes.add(Node(
          DestinationRelay('asd-p1-server3.asd.icaprojecten.nl', 25010),
          'mockKey03'));

      //act
      final List<Node> listOfNodes = await _routeModule.getAndValidateNodes(3);

      //assert
      expect(listOfNodes.length, nodes.length);
    });

    test('Length of path bigger than amount of nodes', () async {
      //arrange
      final RouteModuleImpl _routeModule = RouteModuleImpl();
      //act & assert
      expect(() => _routeModule.getAndValidateNodes(4), throwsArgumentError);
    });
  });
}
