import 'package:asd/bloc/message/bloc.dart';
import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/shared/models/message.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persist_messages.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';

class PersistMessagesMock extends Mock implements PersistMessages {}

void main() {
  final Injector injector = Injector.appInstance;

  setUp(() {
    injectorBootstrap();
    injector.removeByKey<PersistMessages>();
    injector.registerSingleton<PersistMessages>((_) => PersistMessagesMock());
    final PersistMessages mock = injector.getDependency<PersistMessages>();

    when(mock.getMessageList(any)).thenAnswer((Invocation realInvocation) =>
        Future<List<Message>>.delayed(const Duration(), () {
          final List<Message> messages = <Message>[
            TextMessage(
              null,
              DateTime.parse('2018-05-28 13:18:00z'),
              '1',
              "Whats'up?",
            ),
            TextMessage(
              'Tim Hemmes',
              DateTime.parse('2018-05-28 13:18:00z'),
              '1',
              'Hi Jason. Sorry to bother you. I have a queston for you.',
            ),
          ];
          return messages;
        }));
  });

  tearDown(() => injector.clearAll());

  group('test bloc events', () {
    test('initialize bloc', () async {
      final MessageBloc bloc = MessageBloc();
      await emitsExactly(bloc, <Object>[]);
      expect(bloc.state.props.length, 0);
    });

    test('fetch messages', () async {
      final MessageBloc bloc = MessageBloc();
      bloc.add(FetchMessages());
      await emitsExactly(bloc, <Object>[isA<MessageLoaded>()]);
      final List<Message> messages = <Message>[
        TextMessage(
          null,
          DateTime.parse('2018-05-28 13:18:00z'),
          '1',
          "Whats'up?",
        ),
        TextMessage(
          'Tim Hemmes',
          DateTime.parse('2018-05-28 13:18:00z'),
          '1',
          'Hi Jason. Sorry to bother you. I have a queston for you.',
        ),
      ];
      expect(bloc.state.props.length, 1);
      expect(bloc.state.props[0], messages);
    });

    test('add message to uninitialize bloc', () async {
      final MessageBloc bloc = MessageBloc();
      bloc.add(
        IncomingMessage(
          message: TextMessage(
            'Tim Hemmes',
            DateTime.now(),
            '1',
            'Hello, World',
          ),
        ),
      );

      await emitsExactly(bloc, <Object>[]);
      expect(bloc.state.props.length, 0);
    });

    test('add message to loaded bloc', () async {
      final MessageBloc bloc = MessageBloc();
      bloc.add(FetchMessages());

      final List<Message> messages = <Message>[
        TextMessage(
          'Tim Hemmes',
          DateTime.now(),
          '1',
          'Hello, World',
        ),
        TextMessage(
          null,
          DateTime.parse('2018-05-28 13:18:00z'),
          '1',
          "Whats'up?",
        ),
        TextMessage(
          'Tim Hemmes',
          DateTime.parse('2018-05-28 13:18:00z'),
          '1',
          'Hi Jason. Sorry to bother you. I have a queston for you.',
        ),
      ];

      bloc.add(
        IncomingMessage(message: messages[0]),
      );
      await emitsExactly(
          bloc, <Object>[isA<MessageLoaded>(), isA<MessageLoaded>()]);

      expect(bloc.state.props.length, 1);
      expect(bloc.state.props[0], messages);
    });
  });
}
