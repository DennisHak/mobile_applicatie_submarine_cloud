import 'package:asd/bloc/message/bloc.dart';
import 'package:asd/bloc/message/message_bloc.dart';
import 'package:asd/injector_bootstrap.dart';
import 'package:asd/modules/controller/controller_module.dart';
import 'package:asd/modules/shared/models/contact.dart';
import 'package:asd/modules/shared/models/text_message.dart';
import 'package:asd/modules/shared/persistence/persist_contacts.dart';
import 'package:asd/screens/chat_conversation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:injector/injector.dart';
import 'package:mockito/mockito.dart';
import 'package:pointycastle/export.dart';

class ControllerModuleMock extends Mock implements ControllerModule {}

class PersistContactsMock extends Mock implements PersistContacts {}

void main() {
  final Injector injector = Injector.appInstance;
  MessageBloc messageBloc;

  setUp(() {
    injectorBootstrap();
    injector.removeByKey<ControllerModule>();
    injector.removeByKey<PersistContacts>();

    injector.registerSingleton<ControllerModule>((_) => ControllerModuleMock());
    injector.registerSingleton<PersistContacts>((_) => PersistContactsMock());
    messageBloc = MessageBloc();
  });
  tearDown(() => injector.clearAll());

  void setupPersistContactsMock() {
    final PersistContacts mock = injector.getDependency<PersistContacts>();
    when(mock.getUserData()).thenAnswer((Invocation realInvocation) {
      return Future<Contact>.delayed(const Duration(), () {
        return const Contact('test user', null);
      });
    });
  }

  ControllerModule getController() {
    final ControllerModule controllerModule =
        injector.getDependency<ControllerModule>();
    return controllerModule;
  }

  group('Send message', () {
    testWidgets('Widgettest of send message widget',
        (WidgetTester tester) async {
      setupPersistContactsMock();
      await tester.runAsync(() async {
        await tester.pumpWidget(
          createWidgetForTesting(
            child: const ChatConversation(title: 'Confidential Messenger'),
            messageBloc: messageBloc,
          ),
        );

        expect(find.byType(TextField), findsOneWidget);
        expect(find.widgetWithIcon(TextField, Icons.send), findsOneWidget);
        expect(find.widgetWithIcon(TextField, Icons.insert_drive_file),
            findsOneWidget);
      });
    });

    testWidgets('Test if textfield gets filled correctly',
        (WidgetTester tester) async {
      setupPersistContactsMock();
      await tester.pumpWidget(
        createWidgetForTesting(
          child: const ChatConversation(title: 'Confidential Messenger'),
          messageBloc: messageBloc,
        ),
      );
      final Finder messageTextfield = find.byType(TextField);

      await tester.enterText(messageTextfield, 'test message');

      expect(find.text('test message'), findsOneWidget);
    });

    testWidgets('Test if textfield is cleared after tapping on button',
        (WidgetTester tester) async {
      setupPersistContactsMock();
      await tester.pumpWidget(
        createWidgetForTesting(
          child: const ChatConversation(title: 'Confidential Messenger'),
          messageBloc: messageBloc,
        ),
      );

      final Finder sendMessageButton =
          find.widgetWithIcon(TextField, Icons.send);
      final Finder messageTextfield = find.byType(TextField);

      await tester.enterText(messageTextfield, 'test message');
      await tester.tap(sendMessageButton);

      equals(messageTextfield, null);
    });

    testWidgets(
        'Test if sendmessage function is called after tapping on button',
        (WidgetTester tester) async {
      setupPersistContactsMock();
      await tester.runAsync(() async {
        const String testMessage = 'test message';

        await tester.pumpWidget(
          createWidgetForTesting(
            child: const ChatConversation(title: 'Confidential Messenger'),
            messageBloc: messageBloc,
          ),
        );

        final ControllerModule controllerModule = getController();

        final Finder messageTextField = find.byType(TextField);
        await tester.enterText(messageTextField, testMessage);

        final Finder sendMessageButton =
            find.byKey(const Key('SendMessageButton'));
        await tester.tap(sendMessageButton);

        final dynamic callParam =
            verify(controllerModule.sendMessage(captureAny, captureAny))
                .captured;

        expect(callParam is List, true);
        if (callParam is List) {
          expect(callParam.length, 2);

          expect(callParam[0] is TextMessage, true);
          if (callParam[0] is TextMessage) {
            expect(callParam[0].sender, 'test user');
            expect(callParam[0].message, testMessage);
          }

          expect(callParam[1] is Contact, true);
          if (callParam[1] is Contact) {
            expect(callParam[1].alias, 'receiver');
            expect(callParam[1].publicKey, isInstanceOf<PublicKey>());
          }
        }
      });
    });

    testWidgets(
        'Test if sendmessage function is called after '
        'tapping on button while textfield is not filled in',
        (WidgetTester tester) async {
      setupPersistContactsMock();
      await tester.runAsync(() async {
        await tester.pumpWidget(
          createWidgetForTesting(
            child: const ChatConversation(title: 'Confidential Messenger'),
            messageBloc: messageBloc,
          ),
        );
        final ControllerModule controllerModule = getController();
        final Finder sendMessageButton = find.byKey(
          const Key('SendMessageButton'),
        );

        await tester.tap(sendMessageButton);
        verifyNever(controllerModule.sendMessage(any, any));
      });
    });
  });
}

Widget createWidgetForTesting({Widget child, MessageBloc messageBloc}) {
  return MaterialApp(
    home: BlocProvider<MessageBloc>(
      create: (BuildContext context) => messageBloc,
      child: child,
    ),
  );
}
